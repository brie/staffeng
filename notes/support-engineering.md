# Support Engineering

> ...on Support Engineering in general. 

  - [How Support Engineering is bridging technology and business](https://www.metacareers.com/life/how-support-engineering-is-bridging-technology-and-business)
  - [The Backbone of SaaS companies: Support Engineering](https://www.bugsnag.com/content/what-is-customer-engineering)
  - [Simple Rules to Make Support Engineering Painless](https://nordicapis.com/simple-rules-to-make-support-engineering-painless/)
