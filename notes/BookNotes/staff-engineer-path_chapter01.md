# The Staff Engineer's Path: Intro and Chapter 1 👋

February 2023 

These are my notes on the first reading for our book club on **The Staff Engineer's Path**: A Guide for Individual Contributors Navigating Growth and Change by Tanya Reilly. This covers **Introduction** and the first chapter.

## Introduction

  - a path less well-defined
  - uncertainty lies this way
  - expectations may vary within a single company
  - it depends (sounds familiar!)

### Three Pillars

  - Big-picture thinking
  - Execution
  - Leveling up
    - the self... 
    - and engineers in one's orbit

The pillars rest on a **critical** foundation of technical expertise. As always: technical skills are important but are not the only important thing.

### Humaning Skills Include

  - communication and leadership
  - navigating complexity
  - putting your work in perspective
  - mentorship, sponsorship and delegation
  - frame a problem so others care!
  - acting like a leader
    - whether or not you _feel_ like one

## 1: What would you say you do here?

Because there are many different valid types of staff engineer, it's important to understand how to characterize, describe and understand one's own role.

### What even _is_ a staff engineer?

  - Keep growing and stay technical!

For staff engineers and beyond (🚀), we use `staff+`. 

### Why do we need big-picture engineers?

  - Good decisions need context. 
    - It depends.
    - Context helps you through that.
  - Optimize for _all_...
    - not just the local maximum.

#### Avoid local maxima

  - Take an ousider view.
  - Consider the big picture today and down the road.

  Sometimes the right answer is the thing that is slightly annoying now but avoids problems in the future.

  - A year from now: what will you regret?
  - In three years, what will you wish you had done?

#### Management authority can overshadow technical judgment

#### To keep things moving when they otherwise might be stuck

### Why do we need engineers who are a good influence?

  - What we are doing **matters**.
  - Norms are set by the more respected engineers. 
    - If they flaunt standards, other folks likely will, too.

### This is not in addition to the workload of a senior engineer

  - Think "instead" and not "in addition"

### Common Attributes

  - A leader, not the manager
  - "someone should do something here"
    - (That someone is probably you.)
  - Communicate often and well  

You'll take on projects that **can not** succeed without collaboration, communication and alignment.

Designing "happy path" solutions that help protect other engineers from common mistakes.

#### A Technical Role

  - understand tradeoffs when making decisions
    - and help others to understand
  - dive in when necessary
    - Protect your valuable time.
  - ask the right questions
  - The outcome matters more than how you get it

#### An Aim to be Autonomous

  - You generate that magical list of high-impact work
    - with context from others
    - your expertise
  - Defend and structure your time.
    - Weigh benefits, priority, time commitment, relationship
    - There are only so many hours in the day
    - Use your "no" to protect your "yes"

#### On Technical Decisions

  - Ensure they are made. 
    - It's not all up to you. 
  - Eneusre they are made well.
    - It's a team effort.  
  - Ensure they are written down.

#### Reporting Chains Impact

  - level of support received
  - information you're privy to
  - how you're perceived
  - your perspective
  - your scope

 Represent folks who don't have the leverage to do things like prevent poor technical (or policy/process) decisions. Be their voice. 

### On Scope

  - Prepare to ignore it during a crisis

#### Too Broad

  - lack of impact
  - becoming a bottleneck
  - decision fatigue
  - missing relationships

#### Too Narrow

  - lack of impact
  - opportunity cost
  - overshadowing other engineers
  - overengineering

### What Shape is your role?

  - Are you depth-first or breadth-first?
  - Are you OK with delayed gratification?
  - Keeping one foot on the manager track?
  - What's your primary focus?
    - What needs you?
    - What is important?

Your work should be important. Folks might not yet have a mental model for the work or why it's important: you'll need to comunicate that.

You should know why what you are doing is strategically important and be able to articulate that. 

The work plan on pages 28 and 29 is excellent.

  - Your job is to make your organization successful

Things will be a bit weird: that is OK as long as you are in alignment and clear on scope, goals and the general direction.

#### The Four Disciplines

  - Core technical skills
  - Product management
    - what needs to be done?
    - why?
    - maintain the narrative
  - Project management
    - notice what's blocked and **unblock**
  - People management
    - from a group of people to a team
    - build their skills and careers
    - mentoring


# Notes during Book Club

  - Meet people!
    - This requires intention. 
    - birds of a feather
    - This requires effort.
  - No more five minute coffee chats.


### Time Management

On how much time is spent in coffee chats or meetings in general:

  - 💡 Book all meetings on one day. This forces a quota. 



### :snowflake: Staff engineers are like snowflakes.

   - It depends.
  - It's a boundary you can (and probably should) push on!
  - Random pings increase as your expertise becomes recognized and you become a trusted partner. 
    - Do. 
    - Do quickly.
    - Defer.
    - Delegate.


## Additional Reading

  - [StaffEng.com](https://staffeng.com)
  - [levels.fyi](https://levels.fyi)
  - [Kind Engineering](https://kind.engineering/)
  - [Staff archetypes](https://staffeng.com/guides/staff-archetypes)
  - [Staff Engineer: Leadership beyond the management track](https://staffeng.com/book)
  - [The Effects of Employee Job Titles on Respect Granted by Customers](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3027028)
  - [Engineering growth: framework overview](https://medium.com/s/engineering-growth-framework/engineering-growth-framework-overview-4e02ab330524)
  - [Building a manager Voltron](https://larahogan.me/blog/manager-voltron/)
  - [IC Career Track: Job Titles and Roles in Tech](https://alphalist.com/blog/ic-career-track-job-titles-and-roles-in-tech)
