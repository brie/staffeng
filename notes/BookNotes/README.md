# 📚 Book Notes

👋 Hello! These are notes I took while reading [The Staff Engineer's Path](https://www.oreilly.com/library/view/the-staff-engineers/9781098118723/) in spring 2023. In addition to the chapter-by-chapter notes, I have a few of my [[key-takeaways | 🔑 key takeaways]] from the entire work. I read the book as a part of a [book club](https://about.gitlab.com/handbook/leadership/book-clubs/) at [work](https://gitlab.com/).


  - [[staff-engineer-path_chapter01 | Intro and Chapter 1]]
  - [[staff-engineer-path_chapter02 | Chapter 2]]
  - [[staff-engineer-path_chapter03 | Chapter 3]]
  - [[staff-engineer-path_chapter04 | Chapter 4]]
  - [[staff-engineer-path_chapter05 | Chapter 5]]
  - [[staff-engineer-path_chapter06 | Chapter 6]]
  - [[staff-engineer-path_chapter07 | Chapter 7]]
  - [[staff-engineer-path_chapter08 | Chapter 8]]
  - **upcoming** [[staff-engineer-path_chapter09 | Chapter 9]]

## 📖 readMORE

For a list of links to all of the things that the author references in the book, see:

- 🔖  [STAFF ENGINEER’S PATH RESOURCE LIST](https://noidea.dog/staff-resources)

In April 2023, I wrote this blog post based on my time in the book club:

- [A Staff (Support) Engineer's Path](https://brie.dev/staff-engineer-path/)
