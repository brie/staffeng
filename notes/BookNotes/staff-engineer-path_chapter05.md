---
markmap:
  color:
    - '#3066BE'
    - '#FF99C9'
    - '#CFFFB0'
    - '#05A8AA'
    - '#3066BE'
    - '#d5e5ee'
    - '#36213E'
    - orange
    - yellow
    - green
  colorFreezeLevel: 2
  duration: 400
  maxWidth: 800   # Decrease to enable wrapping
  initialExpandLevel: 3
---

# Chapter 5: Leading Big Projects 🚛