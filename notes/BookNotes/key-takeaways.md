---
created: 2023-02-23T02:35:12-05:00
modified: 2023-02-24T23:52:29-05:00
---

# 🔑 Key Takeaways

These are some of the 🔑 key takeaways from **The Staff Engineer's Path** for me. 

On relationships outside your team and organization: **establish** and **maintain** them. 

Does what you are doing right now _matter_? Articulate how. 

## "Someone should do something"

If you look at a situation and think "someone should do something", that someone is probably you. You don't have to do _everything_ and you should not do ANY thing but you should do something. 
 
## Using your influence for good

Be **approachable**. It should be "worth it" for folks to come to you. 

  - be helpful 
    - don't info dump but...
    - answer the direct question and any implicit asks
  - help individuals, help the team
  - empower others to  help others
  - be the colleague you'd like to have

### At Scale

This can be done through **writing** and **public speaking** for most. Other opportunities vary by organization and by the individual. 

You're an influencer now, sorry.

The three parts were:

  - The Big Picture
	  - where are we? where are we going? are we going together?
  - Execution
  - Leveling Up
	  - the team as well as yourself

