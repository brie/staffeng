---
markmap: '{
   color: [#3066BE, #FF99C9, #CFFFB0, #05A8AA, #3066BE, #d5e5ee, #36213E, orange, yellow, green],
   colorFreezeLevel: 2,
   duration: 400,
   maxWidth: 800,
   initialExpandLevel: 3
}'
---

# Chapter 7: You're a Role Model Now (Sorry) 🥺

Be the colleague you'd like to have. 

  - **Q**: How do we balance this with being human? 

How to be a staff engineer without needing to be superhuman?

This is so important to be aware of! It used to surprise me but observing evidence of my stature as a role model to some has encouraged me to step my game up even further. 

## Be Competent

On **building experience**: no one is born a  skilled computer engineer. 

I remind myself and others all the time: no one was born knowing Kubernetes. Ever ask a baby about `kubectl`?

> Do a whole lot of soul-searching before taking a role that takes you further from the tech. 

I feel this! I was at an org where I was being encouraged in that direction. I saw others go for it and a few months later they all said the same thing "they longed to be back at the keyboard". 

I've always known that I was interested in leadership without the management. I'm so grateful for this community around staff+.  


### Own your mistakes

## Be responsible

You are the someone who should do something. 


  - taking ownership
  - taking charge
  - creating calm

Ask "obvious" questions. It's easier for more senior folks to ask the "obvious" question out loud. 

The questions so obvious that nobody else is willing to ask them. 


Don't delegate through neglect.

THIS IS MY FAVORITE CHAPTER. 

I think that anyone should read it!
