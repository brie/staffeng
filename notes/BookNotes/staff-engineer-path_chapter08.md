---
markmap: '{
   color: [#3066BE, #FF99C9, #CFFFB0, #05A8AA, #3066BE, #d5e5ee, #36213E, orange, yellow, green],
   colorFreezeLevel: 2,
   duration: 400,
   maxWidth: 800,
   initialExpandLevel: 3
}'
---

# Chapter 8: Good Influence at Scale ⚖️

Being a good influence as doing the best work you can in a way that others can see.

Be a good influence 

Help  **other** engineers do better work.

  - If you can help colleagues become better engineers: you'll be working with more competent people.
  - As your scope grows, it's harder to have influence through individual interactions.

## Three Tiers of Influence

  - Individual
  - Group
  - Catalyst  

## 4 Ways to Influence

### Advice
#### Individiual Advice

Solicited advice, where possible. There are times when it's  right to offer to share unsolicited advice.

Mentoring as 

> sharing your experience so an engineer can leverage it themselves

As appropriate, set guidelines and goals for the mentoring relationship. (Don't sit there wondering "what are we here for?".)

#### Answering questions

  - Make reaching out to you worth the effort.

Not an info dump but if there's an implicit ask: take care of that, too. The example in the book _hurts_. 

What works for one person might be terrible advice for another! Keep this in mind both when **offering** and when **receiving** avice.

Consider the "be more assertive" <-> "you're too abrasive" line that's far too thin and blurry. (See [The Fine Art of Female Assertiveness](https://www.psychologytoday.com/us/blog/wander-woman/201011/the-fine-art-of-female-assertiveness).)

#### Scaling your Advice to a Group

  - ✅ Give a tech talk. 
  - ✅ Write an article.

If you want to tell somethign to more people, write it down!

If you write down something that applies to peple outside your company, share it further accordingly.

#### Being a Catalyst

  - Set up advice flows that don't **need** you to be involved.
  - Make it easy for colleagues to help each other.

### Teaching

It's not just a senior person teaching a junior person. It's for any time there's a knowledge gap. Some of the best knowledge-gap teaching is informal and comes from pairing sessions. 

Getting docs updates out of it is a good goal for these kinds of situations. (The docs weren't sufficient to close the knowledge gap, hence the session.)

#### Unlocking a topic

What kinds of scenarios should they be able to handle by the end of the session?

Successful teaching engages the learner who should be **doing** as well as listening. 

  - There should be an artifact to refer to: code snippet, docs update, et cetera. 
    - For me, it's notes. 

#### Review

  - Can be a confidence booster!
  - Can be the opposite, too: be kind, take care with your words.

  - Say **why** as well as _what_.
  - Choose your battles.
  - Say what you mean.

#### Coaching

  - Ask open questions
  - Listen actively
  - Make space

### Guardrails

#### Individual

  - code/design review
  - support during big projects
  - change management!

💖 The list of things to look for on page 271 is amazing!

When acting as a project guardrail: be **specific** about what you can and will do to help.

The folks acting as guardrails should help point out when one might be too close to a cliff: hmm, how are you planning to balance `x` and `y`?

#### Group

Don't ever rubber-stamp changes...right, but you can't review _every_ change for _every_ project. 

##### Processes

...but not for it's own sake.

  - processes as lightweight as possible
  - make the right way also the easy way

##### Written Decisions

  - style guides
  - paved roads
  - policies
  - technical vision and strategy

Don't you love when you find just the right style guide or policy doc for the situation at hand? The [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide/) has saved me _so_ much time and has saved the people I would have asked time, too.

###### Robots and Reminders

  - Make it as easy as possible to do (or remember to do) the right thing.

### Opportunity

#### Individual Opportunities

Find ways to get people learning by doing. This can be done through:

  - Delegation
  - Sponsorship
  - Connecting people

On **connecting people**, you can offer opportunities simply by knowing they exist. You're meeting people and getting to know the organization (and it's needs) outside your team. Bring others along with you.

#### Opportunities for the Group

  - Share the spotlight

Instead of overshadowing everyone, share the stage. Ensure that _others_ have the room to grow into leadership.

⚠️ There's a balance to be struck in how much direct execution you do and how much work you do supporting the team. (Oversimplified: don't delegate _everything_.)

## 🎁 Wrap Up

Not that you need them, but these are four reasons to build up your team:

  - get more done
  - keep tech up-to-date
  - improve the industry
  - ✨ other people's growth is _your_ growth ✨

> The more your colleagues can do, the more you can do.

Be a good influence one-on-one and beyond: writing and public speaking can send your messsage much farther.

Guardrails can let people work more autonomously. (When consructed properly: they should be guardrails and not roadblocks.)

## ❓ Questions

  - How do you make sure that guardrails are not roadblocks?
