---
markmap:
  color:
    - '#3066BE'
    - '#FF99C9'
    - '#CFFFB0'
    - '#05A8AA'
    - '#3066BE'
    - '#d5e5ee'
    - '#36213E'
    - orange
    - yellow
    - green
  colorFreezeLevel: 2
  duration: 400
  maxWidth: 800   # Decrease to enable wrapping
  initialExpandLevel: 3
---

# Chapter 4: Finite Time ⏰

There will _always_ be something that needs to be fixed.

You can't fix every problem: decide which problems you will fix. 

Side quests are a lot of fun but life is _not_ a video game that you can 100%. Will you do `a` or `b`?   

## Doing All the Things

Choose projects that support your growth, reputation and happiness.

Every week has 168 hours.

In addition to your time, other resources to manage include: energy, quality of life, credibility, social capital and skills. 

## Time

### Finite Time

Typically, work calendars show meetings, the **interruptions** to work. 

  - Put **work** on your calendars (in addition to meetings). 

This makes it really easy to see whether you can afford to tap `y` on that side quest.  

### How busy do you like to be?

Can you handle something extra without melting down?

  - Use the maps we've discussed thus far to guide these decisions.

### Choosing work

Oof. 

> There will always be a balance between choosing the strictly most important next thing **and** choosing the work that's right for you.

Taking care of one's own needs can be compatible with being a team player!

### Resource Constraints 

#### Credibility

  - Can be built by
    - solving hard problems
    - being visibly competent
    - consistently showing good technical judgment


  - Balance
    - seeing the big picture
    - accepting **pragmatic** local solutions

#### Skills

Don't take on projects that teach you onothing (relevant/useful).

  - Increased by
    - structured learning
      - take a class
      - read a book
      - a well-scoped project
    - working closely with someone skilled
    - by doing  
      - be hands-on
      - spend time
