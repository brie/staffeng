---
markmap:
  color:
    - '#3066BE'
    - '#FF99C9'
    - '#CFFFB0'
    - '#05A8AA'
    - '#3066BE'
    - '#d5e5ee'
    - '#36213E'
    - orange
    - yellow
    - green
  colorFreezeLevel: 2
  duration: 400
  maxWidth: 800   # Decrease to enable wrapping
  initialExpandLevel: 3
---

# Chapter 3: Creating the Big Picture 🎞

This is for when the goal is not clear (to all) or the plan is disputed. The **group** works together to build the _technical vision_ or _technical strategy_. (Buy-in is important.)

## Case Study: Sockmatcher

Technical decisions are often made for non-technical reasons: I _hate_ it but it's reality.

## What's a Vision? What's a Strategy?

### How to know you might need one or a new one

Decentralized decision-making: great for that team (local maximum). Tragedy of the commons.

Oof: neglected shared concerns because unclear ownership means no one has the authority or incentive to fix them alone. It's "hard" so it does not get done. 

### What is a technical vision?

  - Where we are going.

The future as you would like it to be: forget about the details, this is how things _should_ look when we are done. Isn't it pretty?

The scope can vary widely from "the whole engineering org" to "a team". (If it's narrow, make sure it aligns with everything else that's going on.)

  - A vision creates a shared reality.
    - It **guides** decision-making.
    - It shoudl reduce ambiguity and empower people.

### What is a technical strategy?

  - ...how we will get there.

It should include challenges we'll face; it might provide guidance on priority.

#### The diagnosis

The most essential characteristics of the current state. The current state is a messy reality so look for patterns and the essentials: this is hard.

#### Guiding policy

# Recommended Reading

  - [How to Set the Technical Direction for Your Team](http://jlhood.com/how-to-set-team-technical-direction/)
  - [Notes from “Good Strategy / Bad Strategy”](https://jlzych.com/2018/06/27/notes-from-good-strategy-bad-strategy/)