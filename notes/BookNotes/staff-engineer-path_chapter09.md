---
markmap:
  color:
    - '#3066BE'
    - '#FF99C9'
    - '#CFFFB0'
    - '#05A8AA'
    - '#3066BE'
    - '#d5e5ee'
    - '#36213E'
    - orange
    - yellow
    - green
  colorFreezeLevel: 2
  duration: 400
  maxWidth: 800   # Decrease to enable wrapping
  initialExpandLevel: 3
---

# Chapter 9: What's Next? 🚀

> 🎥 [Chapter 9 Discussion on YouTube](https://youtu.be/ygYyvPVwsRk)

The paths onward are loosely defined. 

## Your Career

Many of the most interesting places can only be seen if you leave the trail. It's OK to trailblaze. 

### What's important to **you**?

  - 📚 learning
  - 🏆 being challenged
  - 🎉 helping others succeed! 

### What do you need to invest in?

  - building skills
  - building a network
  - building visibility

Let's say you have a _finite_ number of skill points that you earn every year. What will you spend them on?

  - choosing roles and projects deliberately

## Your Current Role

### Five Metrics to Keep an Eye On

  - whether you're learning
  - whether you’re investing in transferable skills or navigating dysfunction
  - how you feel about recruiting other people to your team
  - How confident you feel
  - How stressed you feel

## Paths from Here

## Your Choices Matter

 
