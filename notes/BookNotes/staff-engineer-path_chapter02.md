---
markmap:
  color:
    - '#3066BE'
    - '#FF99C9'
    - '#CFFFB0'
    - '#05A8AA'
    - '#3066BE'
    - '#d5e5ee'
    - '#36213E'
    - orange
    - yellow
    - green
  colorFreezeLevel: 2
  duration: 400
  maxWidth: 800   # Decrease to enable wrapping
  initialExpandLevel: 3
---

# Chapter 2: Three Maps 📍

February 2023

Context is 🔑 key. 

Know where you stand and understand your surroundings.

Retain facts for later: think about how things are connected. (marketing push --> traffic spike)

## What are the maps?

### Locator Map

When you zoom (way) out: how important is what you're doing? Does it show up on the map? This can be hard to understand when you're very close to the work. 

  - Let the org chart _inform_ this

### Topographical Map

Know the terrain. Understand how decisions are made, how folks prefer to work. Seek existing navigable paths. 

(Trail blazing is OK, too.)

It occurs to me that all of these maps change with time.

### Treasure Map

This one has a destination and a few stops along the way. 

  - Are you moving in the general direction of where you want to be?
  - Is project `y` a goal in itself -- or does it help  us get to the treasure?

## Locator Map: Finding Perspective
### Focus: risk and reward

Focus brings depth and understanding. There are risks:

  - Poor prioritization
    - Avoid local maxima
    - No wheel reinvention
  - Losing empathy
    - too absorbed in incident details: users are waiting
    - [XKCD 2501](https://xkcd.com/2501): Average Familiarity
  - Tuning out the background noise
    - Don't get so used to something bad, you forget it's a problem. 
    - Is that problem on the way to a crisis?
  - Forgetting what the work is for
    - Everyone working on their own part...
      - ...without confirming "does this even still matter?"
    - The treasure map should help with this!

### The Outsider View

The observations of folks new to the organization are always super valuable: they can easily see that with which we have been living. All the same, there's sometimes a really good reason why something somehwat suboptimal in place. 

I adore the Amazon principle engineer group community tenet

  - Respect what came before
    - Solve the problem: writing code might not be the way
    - Study what others have done before you build

They [elaborate](https://www.amazon.jobs/en/landing_pages/pe-community-tenets):

Principal Engineers are grateful to our predecessors. We appreciate the value of working systems and the lessons they embody. We understand that many problems are not essentially new.

While I certainly agree, I might encourage an element of "respect but wonder". 

  - No thanks: "This is awful; I can't believe it's built this way!"
  - Better: "I wouldn't have made this decision: can you please say more about why this is?"

IMO, this is an exercise in positive disagreement. I **highly** [recommend](https://brie.dev/2023-january-articles-notes-commands/) reading [Conversation Skills Essentials](https://tynan.com/letstalk/) for more.

  - Think beyond your organization. 
    - Read publications
    - Networking. 

#### Escaping the echo chamber

Seek disagreement! It's exciting and does _not_ have to be a negative or bad thing.

I recommend **curiosity** when the disagreement is found. Understanding why someone believes something can be so important. 

You might learn something to help bolster a theory. 
You might learn something that will totally change your opinion or approach on something. 

  - Build friendly relationships with other staff engineers. 
  - Think of them as _your team_ as much as any other team. 
    - It's the truth!

Doing this deliberately helps you to proceed as if you're part of something bigger than your own team. Because you are. 

Thinking big picture: this relationship-building should not be limiited to engineering peers!

#### Always True Objectives

These typically are not stated unless they are in danger.

  - Continue to exist
  - Have enough $ to pay everyone
  - Have a functional product/service
  - Have a thing that customers _want_ to buy

### What do customers actually care about?

  - Nines don't matter when users aren't happy

Many customers won't _care_ that the problem is in some upstream provider, limited in scope, whatever. What they know is that it doesn't work and they get it through you. 

  - Measure success from the **customer's** point of view

## Topographical Map: Navigating Tricky Terrain

The locator map provides perspective but does not permit navigation: it lacks information about the terrain.

The team tectonics analogy is memorable.

### Why terrain matters

The landscape can change (very rapidly).

  - Being right is only part of the battle: convincing others why you're right and why they should care
  - You might not see the obstacle until you are upon it.
  - Time your initiatives well
    - There are other things going on: work with them, rather than against

### Understanding your Organization

_WOW_. On what people mean when they ask what the culture is like: autonomy, inclusion, how safe it is to make mistaks all come up. Also: are people nice? (It goes such a very long way.)

  - Values are **aspirational**. 
    - Important...
    - Consider what actualy happens, too

On **secret or open** cultures: I know where I want to be but an open culture has risks. There is so much to consume. Decision fatigue! Things get missed! Drama!

  - Know the cultural expectations around sharing information

Where do initiatives come from?

  - [Is tech a meritocracy?](https://istechameritocracy.com/)
    - No, BTW.

The sliders we reviewed:

  - Secret <-> Open
  - Oral <-> Written
  - Top-down <-> bottom-up
  - Fast <-> deliberate
  - Back channels <-> Front doors
  - Allocated <-> Available
  - Liquid <-> Crystallized

This isn't about judging: it's about understanding how things are where you are. 

On **pathological**, **bureaucratic** and **generative** organizations...

### Points of Interest

What gets lost in the gaps between teams?

  - Chasms
  - Fortresses
    - Gatekeepers _care_. 
  - Disputed territory
   - "I think so but you should also ask..."
  - Uncrossable deserts
    - It's OK to try.
    - Is it worth it, though?

### BE A BRIDGE.

The better you know the terrain, the better you can help others safely navigate. 

Build bridges _preemptively_: get to know folks whose work you don't understand.

## Treasure Map: Remind Me Where We're Going

Where we are going and why.

  - Take the long view.
  - Share the map.

Sometimes the map must be redrawn.

# Book Club Qs and Notes

  - It's weird reading about what someone did during the pandemic in print!
