# 🪄 Staff Support Engineers

## 💼 Job Descriptions

The first chapter of **The Staff Engineer's Path** asks "what would you say you do here?". One way to start to understand how that question is answered is to look at job descriptions. Here are the publicly available job descriptions for **Staff Support Engineer** and similar documents:

  - [Staff Support Engineer](https://about.gitlab.com/handbook/support/engineering/staff.html)
  - [Support Career Framework: Staff Support Engineer](https://about.gitlab.com/handbook/engineering/career-development/matrix/engineering/support/staff/)

Gigamon:
  - [Staff Technical Support Engineer](https://archive.ph/owMp9) -> see [Senior](https://archive.ph/exnbc) for comparison

SentinelOne:
  - [Staff Support Engineer](https://archive.ph/OTnpX)

## ✍️ Blog Posts and 🎥 Videos

  - [Group AMA - Being/Becoming a Staff Support Engineer - 2022-05-26](https://www.youtube.com/watch?v=68jzHfG8SwU) 
  - [Reflection: First third of my fifth year at GitLab and defining Staff Support Engineer](https://cynthiang.ca/2022/11/04/reflection-first-third-of-my-fifth-year-at-gitlab-and-defining-staff-support-engineer/)
  - [Reflection: The last third of my fifth year at GitLab and expanding Staff Support Engineer](https://cynthiang.ca/2023/06/20/reflection-the-last-third-of-my-fifth-year-at-gitlab-and-expanding-staff-support-engineer/)
  - [Discussion on becoming a Staff Support Engineer](https://www.youtube.com/watch?v=dN4LyA4nDg0)
  - [✨ Our Paths to Staff Support Engineer | GitLab Support](https://www.youtube.com/watch?v=50LObql1dx8)
