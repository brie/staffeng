# 👋 Hello, world. 

My name is [Brie Carranza](https://brie.dev). These are my notes on **staff engineering**. Given [who I am](https://www.linkedin.com/in/briecarranza/) these are oriented towards a **Staff Support Engineer** mindset. I started these notes as I read Tanya Reilly's [The Staff Engineer's Path](https://www.oreilly.com/library/view/the-staff-engineers/9781098118723/) as part of a book club. You'll find the videos of some of our discussions on the [Book Club - The Staff Engineer's Path](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp7yMHav5Gwt9_BkOXO90hM) playlist on the **GitLab Unfiltered** channel on YouTube.  

There are [mindmaps](https://brie.dev/2023-markdown-mindmap/) for many chapters in the `maps` subdirectory. Each mindmap is linked directly: 

### The Big Picture

  - [Intro and 1: What would you say you do here?](https://brie.gitlab.io/staffeng/maps/chapter1.html)
  - [2: Three Maps](https://brie.gitlab.io/staffeng/maps/chapter2.html)
  - [3: Creating the Big Picture](https://brie.gitlab.io/staffeng/maps/chapter3.html)

### Leveling Up  
  - [7: You're a Role Model Now (Sorry)](https://brie.gitlab.io/staffeng/maps/chapter7.html)
  - [8: Good Influence at Scale](https://brie.gitlab.io/staffeng/maps/chapter8.html)

The mindmaps are generated using `markmap`. Read my blog post on [making Markdown mindmaps with marmap](https://brie.dev/2023-markdown-mindmap/).